﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    public partial class Form1 : Form
    {
        List<Objcts> objects;
        List<Objcts> results;

        Form2 Adding;
        public Form1()
        {
            InitializeComponent();
            objects = new List<Objcts>();
            
        }

        private void Add_Click(object sender, EventArgs e)
        {
            //Persone
            if (radioButton1.Checked)
            {
                Adding = new Form2(0, objects);
                Adding.Text = "Add persone";
                Adding.ShowDialog();
                objects.AddRange(Adding.newobjs);
            }
            //Machine
            if (radioButton2.Checked)
            {
                Adding = new Form2(1, objects);
                Adding.Text = "Add machine";
                Adding.ShowDialog();
                objects.AddRange(Adding.newobjs);

            }
            //Building
            if (radioButton3.Checked)
            {
                Adding = new Form2(2, objects);
                Adding.Text = "Add building";
                Adding.ShowDialog();
                objects.AddRange(Adding.newobjs);
            }

        }

        private void button1_Click(object sender, EventArgs e)//Search
        {
            listBox1.Items.Clear();
            results =new List<Objcts>();
            try
            {
                if (radioButton5.Checked)
                {
                    string name_s = textBox1.Text;
                    results.AddRange(objects.FindAll(n => n.Name == name_s).ToList());

                }
                if (radioButton6.Checked)
                {
                    int id_s = Convert.ToInt32(textBox1.Text);
                    results.AddRange(objects.FindAll(n => n.ID == id_s).ToList());
                }
            }
            catch { MessageBox.Show("Nothing was found."); }
            if (results.Count==0)
                MessageBox.Show( "Nothing was found.");
            else
                for (int i = 0; i < results.Count; i++)
                    listBox1.Items.Add("Name: " +results[i].Name+" ID: "+ results[i].ID.ToString());

        }

        private void Show_Click(object sender, EventArgs e)
        {
            int i = listBox1.SelectedIndex;
            if (i >= 0)
            {
                Form2 f = new Form2(results[i]);
                f.ShowDialog();
                if (f.newobjs.Count>0)
                {
                    objects.Remove(results[i]);
                    objects.Add(f.newobjs.Last());
                    button1.PerformClick();
                }
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            int i = listBox1.SelectedIndex;
            if (i >= 0)
            {
                objects.Remove(results[i]);
                results.Remove(results[i]);
                button1.PerformClick();
            }
        }
       
    }
}

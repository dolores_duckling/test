﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class Machine : Objcts
    {
        public string Place;
        public DateTime Time;
        public int Age;
        public int Building_id;
        public enum Product_type
        {
            lathes,
            drilling,
            boring,
            grinding
        }
        public Product_type Product;
        public enum Raw_material
        {
            aluminum,
            copper,
            tin
        }
        public Raw_material Material;
        public double Cost;
        public int Productivity;

        public Machine(string name, int id, string place, DateTime time, int building_id, Product_type product, Raw_material material, double cost, int Q)
        {
            Name = name;
            ID = id;
            Place = place;
            Time = time;

            Age = DateTime.Now.Year - time.Year;
            if (time.Date > DateTime.Now.AddYears(-Age)) Age--;

            Building_id = building_id;
            Product = product;
            Material = material;
            Cost = cost;
            Productivity = Q;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    public partial class Form2 : Form
    {
        int obj_num;
        public List<Objcts> newobjs;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2( Objcts a)
        {
            InitializeComponent();
            newobjs = new List<Objcts>();
            Edit.Visible = true;
            button1.Visible = false;
            textBox1.Text = a.Name;
            textBox2.Text = a.ID.ToString();
            if (a is Persone p)
            {
                Preparing(0);
                textBox3.Text = p.Place;
                textBox4.Text = p.Time.ToString("yyyy-MM-dd");
                textBox5.Text = p.Age.ToString();
                textBox6.Text = p.Adress;
                textBox7.Text = p.Height.ToString();
                textBox9.Text = p.Weight.ToString();
                comboBox1.SelectedIndex = (int)p.Gender;
                obj_num = 0;


            }
            if (a is Machine m)
            {
                Preparing(1);
                textBox3.Text = m.Place;
                textBox4.Text = m.Time.ToString("yyyy-MM-dd");
                textBox5.Text = m.Age.ToString();
                textBox6.Text = m.Building_id.ToString();
                textBox7.Text = m.Productivity.ToString();
                textBox9.Text = m.Cost.ToString();
                comboBox1.SelectedIndex = (int)m.Material;
                comboBox2.SelectedIndex = (int)m.Product;
                obj_num = 1;
            }
            if (a is Building b)
            {
                Preparing(2);
                textBox3.Text = b.Place;
                textBox4.Text = b.Time.ToString("yyyy-MM-dd");
                textBox5.Text = b.Age.ToString();
                textBox6.Text = b.Adress;
                textBox7.Text = b.Number_of_storeys.ToString();
                textBox9.Text = b.Cost.ToString();
                comboBox1.SelectedIndex = (int)b.Building_type;
                obj_num = 2;
            }
        }
        public Form2(int index, List<Objcts> obj)
        {
            newobjs = new List<Objcts>();
            InitializeComponent();
            Preparing(index);
            
        }
       
        public void Preparing(int index)
        {
            if (index == 0)//persone
            {
                label9.Text = "Weight";
                comboBox1.Items.Add("Male");
                comboBox1.Items.Add("Female");
                comboBox1.Items.Add("Undefined");
                obj_num = 0;
            }
            if (index == 1)//machine
            {
                label10.Visible = true;
                comboBox2.Visible = true;
                label6.Text = "Building ID";
                label10.Text = "Product type";
                comboBox1.Items.Add("aluminum");
                comboBox1.Items.Add("copper");
                comboBox1.Items.Add("tin");
                label8.Text = "Raw material type";
                label7.Text = "Productivity";
                comboBox2.Items.Add("lathes");
                comboBox2.Items.Add("drilling");
                comboBox2.Items.Add("boring");
                comboBox2.Items.Add("grinding");
                obj_num = 1;
            }
            if (index == 2)//building
            {
                label7.Text = "Floors number";
                label8.Text = "Building type";
                comboBox1.Items.Add("public");
                comboBox1.Items.Add("residential");
                comboBox1.Items.Add("industrial");
                comboBox1.Items.Add("agricultural");
                obj_num = 2;
            }
        }
        private void button1_Click(object sender, EventArgs e)//ADD
        {
            try
            {
                if (obj_num == 0)
                    newobjs.Add(new Persone(Convert.ToString(textBox1.Text), Convert.ToInt32(textBox2.Text), Convert.ToString(textBox3.Text),
                        DateTime.ParseExact(textBox4.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture),
                        Convert.ToString(textBox6.Text), (Persone.Sex)comboBox1.SelectedIndex,
                        Convert.ToInt32(textBox7.Text), Convert.ToInt32(textBox9.Text)));
                if (obj_num == 1)
                    newobjs.Add(new Machine(Convert.ToString(textBox1.Text), Convert.ToInt32(textBox2.Text), Convert.ToString(textBox3.Text),
                       DateTime.ParseExact(textBox4.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture),
                       Convert.ToInt32(textBox6.Text), (Machine.Product_type)comboBox2.SelectedIndex, (Machine.Raw_material)comboBox1.SelectedIndex,
                       Convert.ToInt32(textBox9.Text), Convert.ToInt32(textBox7.Text)));
                if (obj_num == 2)
                    newobjs.Add(new Building(Convert.ToString(textBox1.Text), Convert.ToInt32(textBox2.Text), Convert.ToString(textBox3.Text),
                       DateTime.ParseExact(textBox4.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture),
                       Convert.ToString(textBox6.Text), Convert.ToInt32(textBox7.Text),
                       (Building.Type)comboBox1.SelectedIndex, 
                       Convert.ToInt32(textBox9.Text)));
            }
           catch
            {
                MessageBox.Show("Some parameters is set incorrectly.");
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                if (obj_num == 0)
                    newobjs.Add(new Persone(Convert.ToString(textBox1.Text), Convert.ToInt32(textBox2.Text), Convert.ToString(textBox3.Text),
                        DateTime.ParseExact(textBox4.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture),
                        Convert.ToString(textBox6.Text), (Persone.Sex)comboBox1.SelectedIndex,
                        Convert.ToInt32(textBox7.Text), Convert.ToInt32(textBox9.Text)));
                if (obj_num == 1)
                    newobjs.Add(new Machine(Convert.ToString(textBox1.Text), Convert.ToInt32(textBox2.Text), Convert.ToString(textBox3.Text),
                       DateTime.ParseExact(textBox4.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture),
                       Convert.ToInt32(textBox6.Text), (Machine.Product_type)comboBox2.SelectedIndex, (Machine.Raw_material)comboBox1.SelectedIndex,
                       Convert.ToInt32(textBox9.Text), Convert.ToInt32(textBox7.Text)));
                if (obj_num == 2)
                    newobjs.Add(new Building(Convert.ToString(textBox1.Text), Convert.ToInt32(textBox2.Text), Convert.ToString(textBox3.Text),
                       DateTime.ParseExact(textBox4.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture),
                       Convert.ToString(textBox6.Text), Convert.ToInt32(textBox7.Text),
                       (Building.Type)comboBox1.SelectedIndex,
                       Convert.ToInt32(textBox9.Text)));
            }
            catch
            {
                MessageBox.Show("Some parameters is set incorrectly.");
            }
        }
    }
}

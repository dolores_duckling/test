﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class Objcts
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }

    class Building : Objcts
    {
        public string Place;
        public DateTime Time;
        public int Age;
        public string Adress;
        public int Number_of_storeys;
        public enum Type
        {
            publc,
            residential,
            industrial,
            agricultural
        }
        public Type Building_type;
        public double Cost;

        public Building(string name, int id, string place, DateTime time, string adress, int num_of_floor, Type type, double cost)
        {
            Name = name;
            ID = id;
            Place = place;
            Time = time;

            Age = DateTime.Now.Year - time.Year;
            if (time.Date > DateTime.Now.AddYears(-Age)) Age--;

            Adress = adress;
            Number_of_storeys = num_of_floor;
            Building_type = type;
            Cost = cost;
        }

    }
}

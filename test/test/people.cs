﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    public class Persone : Objcts
    {
        public string Place;
        public DateTime Time;
        public int Age;
        public string Adress;

        public enum Sex
        {
            Male,
            Female,
            Undefined
        }
        public Sex Gender;
        public int Height;
        public int Weight;

        public Persone(string name, int id, string place, DateTime time, string adress, Sex gender, int height, int weight)
        {
            Name = name;
            ID = id;
            Place = place;
            Time = time;

            Age = DateTime.Now.Year - time.Year;
            if (time.Date > DateTime.Now.AddYears(-Age)) Age--;

            Adress = adress;
            Gender = gender;
            Height = height;
            Weight = weight;
        }
    }
    
}
